function d = dual(m,varargin)

switch nargin
    case 2
        nElements = varargin{1};
        d = (nElements-1) - m;
    case 3
        N = varargin{1};
        k = varargin{2};
        
        d = (nchoosek_smart(N,k)-1) - m;
    otherwise
        error('Unsupported number of inputs');
end