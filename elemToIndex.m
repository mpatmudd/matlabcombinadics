function index = elemToIndex(e,N)
k = length(e);
y = 0;
for ii = 1:k
    y = y + nchoosek_smart(N-1-e(ii),k-(ii-1));
end

index = nchoosek_smart(N,k) - 1 - y;