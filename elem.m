function e = elem(m,N,k)
x = dual(m,N,k);
c = comb(x,N,k);
e = (N-1) - c;