function c = comb(m,N,k)

x = m;
a = N;
b = k;

c = nan(k,1);
for ii = 1:k
    c(ii) = largestV(a,b,x);
    x = x - nchoosek_smart(c(ii),b);
    a = c(ii);
    b = b - 1;
end