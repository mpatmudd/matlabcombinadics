function nck = nchoosek_smart(N,k)

if N < k
    nck = 0;
elseif N==k
    nck = 1;
else
    nck = nchoosek(N,k);
end