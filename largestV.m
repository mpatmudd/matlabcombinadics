function v = largestV(a,b,x)

v = a-1;
while(nchoosek_smart(v,b) > x)
    v = v - 1;
end